<?php

namespace Drupal\drd\Event;

/**
 * Event gets dispatched when an action starts.
 *
 * @package Drupal\drd\Event
 */
class DrdActionStart extends DrdBase {

}
