<?php

namespace Drupal\drd\Event;

/**
 * Contains all events triggered by the DRD module.
 */
final class DrdEvents {

  public const ACTION_STARTED = 'drd.action.started';

  public const ACTION_FINISHED = 'drd.action.finished';

}
