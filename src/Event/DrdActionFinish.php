<?php

namespace Drupal\drd\Event;

/**
 * Event gets dispatched when an action finishes.
 *
 * @package Drupal\drd\Event
 */
class DrdActionFinish extends DrdBase {

}
