<?php

namespace Drupal\drd;

/**
 * Provides constants for the library.
 *
 * @package Drupal\drd
 */
class LibraryBuild {

  // Keep this for compatibility reasons.
  public const DRD_LIBRARY_VERSION = '1.7.0';

}
