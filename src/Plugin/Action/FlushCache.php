<?php

namespace Drupal\drd\Plugin\Action;

/**
 * Provides a 'FlushCache' action.
 *
 * @Action(
 *  id = "drd_action_flush_cache",
 *  label = @Translation("Flush Cache"),
 *  eca_version_introduced = "4.1.0",
 *  type = "drd_domain",
 * )
 */
class FlushCache extends BaseEntityRemote {

}
